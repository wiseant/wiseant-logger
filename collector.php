<?php
	define( 'PASSWORD', 'my_test' );

	foreach(array('filename', 'password', 'message', 'uid') as $key)
		if(empty($_POST[$key])) die('key');

	if($_POST['password'] !== PASSWORD) die('password');

	//Support CloudFlare
	$current_ip = isset($_SERVER['HTTP_CF_CONNECTING_IP']) ? $_SERVER['HTTP_CF_CONNECTING_IP'] : $_SERVER['REMOTE_ADDR'];

	$filename = base64_encode(str_replace(array('/','\\','.'), '', urldecode($_POST['filename'])));
	file_put_contents("logs/{$filename}", '"'.implode('";"', array(date('Y.m.d H:i:s'), $current_ip, (int)$_POST['uid'], trim(htmlentities(iconv('cp1251','utf-8',$_POST['message']),ENT_QUOTES))))."\"\n", FILE_APPEND);
?>