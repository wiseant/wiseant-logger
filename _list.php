<?php
$files = array();
if($handle = opendir('logs'))
{
    while (false !== ($entry = readdir($handle)))
    	$files[] = $entry;
    closedir($handle);
}
$files = array_diff($files, array('.', '..'));
?>
<style type="text/css">
	tr td:last-child
	{
		float:right;
	}
</style>
<h3>Список логов</h3>
<?php if(empty($files)):?>
	<center><h4>Пока нет ни одного лога</h4></center>
<?php else: ?>
	<div class="col-lg-8">
		<table class="table">
			<thead><tr><th>Имя лога</th><th>Дата создания</th><th>Дата последнего изменения</th></tr></thead>
			<tbody>
				<?php foreach($files as $file):?>
					<tr><td><?=base64_decode($file)?></td><td><?=date('Y.m.d H:i:s', filemtime("logs/$file"))?></td><td><?=date('Y.m.d H:i:s', filectime("logs/$file"))?></td><td><a class="btn btn-success btn-sm" target="_blank" href="index.php?p=log&filename=<?=urlencode($file)?>">Открыть лог</a></td></tr>
				<?php endforeach;?>
			</tbody>
		</table>
	</div>
<?php endif;?>