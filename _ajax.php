<?php
	if(empty($_POST['action'])) die();

	switch ($_POST['action'])
	{
		case 'delete':
			$filename = !empty($_GET['filename']) ? str_replace(array('/','\\','.'), '', urldecode($_GET['filename'])) : false;
			if(file_exists("logs/$filename"))
			{
				list($ip, $session) = explode('-', $_POST['uid']);
				$file = array();
				if (($handle = fopen("logs/$filename", 'r')) !== FALSE) {
				    while (($parsed_data = fgetcsv($handle, 1000, ';')) !== FALSE) {
				    	if(!($parsed_data[1] == $ip && $parsed_data[2] == $session))
				    		$file[] = '"'.implode('";"', $parsed_data).'"';
				    }
				    fclose($handle);
				}

				file_put_contents("logs/$filename", implode("\n", $file));
				echo 'success';
			}
		break;
	}
?>