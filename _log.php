<nav>
	<ul class="pager">
		<li class="previous"><a href="index.php"><span aria-hidden="true">&larr;</span> К списку логов</a></li>
	</ul>
</nav>
<?php
	$filename = !empty($_GET['filename']) ? str_replace(array('/','\\','.'), '', urldecode($_GET['filename'])) : false;
	if(file_exists("logs/$filename")):?>

		<?php
			$data = array();
			if (($handle = fopen("logs/$filename", 'r')) !== FALSE) {
			    while (($parsed_data = fgetcsv($handle, 1000, ';')) !== FALSE) {

			    	if(empty($parsed_data[0])) continue;
			    	//IP
			    	if(!array_key_exists($parsed_data[1], $data))
			    		$data[$parsed_data[1]] = array();

			    	//Сессия
			    	if(!array_key_exists($parsed_data[2], $data[$parsed_data[1]]))
			    		$data[$parsed_data[1]][$parsed_data[2]] = array('messages' => array(), 'ended' => false, 'success' => true);

			    	//Добавление записи
			    	$ended = strpos( $parsed_data[3], '$end' ) !== false;
			    	if($fail = strpos($parsed_data[3], '$fail') !== false)
			    		$ended = true;

			    	$message = str_replace(array('$fail','$end'), '', $parsed_data[3]);
			    	$data[$parsed_data[1]][$parsed_data[2]]['messages'][] = array( 'message' => $message, 'date' => $parsed_data[0]);

			    	if($ended) $data[$parsed_data[1]][$parsed_data[2]]['ended'] = true;
			    	if($fail)  $data[$parsed_data[1]][$parsed_data[2]]['success'] = false;

			    }
			    fclose($handle);
			}
		?>
		<script type="text/javascript">
		$(function(){
			$('.item-session').click(function(){
				$('.div-session').hide();
				$('[session="'+$(this).attr('target')+'"]').show();
				$('.session-not-checked').hide();
			})

			$('.btn-remove-session').click(function(){
				var uid = $(this).parent().attr('session');
				$.post('index.php?filename=<?php echo $filename?>&p=ajax', {action:'delete', uid:uid},function(html){
					if(html.indexOf('success')+1)
					{
						$('[session="'+uid+'"]').remove();
						$('[target="'+uid+'"]').remove();
					}
				})
			})
		})
		</script>
		<?php if(!empty($data)):?>
			<h3>Лог <?=base64_decode($filename)?></h3>
			<div class="col-lg-7">
				<center class="session-not-checked"><h4>Выберите одну из сессий справа.</h4></center>
				<?php foreach($data as $ip => $sessions):$idx_session = 1;?>
					<?php foreach($sessions as $session_id => $messages): ?>
						<div class="col-lg-12 div-session" session="<?php echo "$ip-$session_id"?>" style="display:none;">
							<a class="btn btn-danger btn-small btn-remove-session">Удалить сессию <?php echo $ip; echo count($sessions) > 1 ? " #{$idx_session}" : ''?></a><hr/>
							<table class="table">
							<?php foreach($messages['messages'] as $message):?>
								<tr><td><?php echo $message['date']?></td><td><?php echo $message['message']?></td></tr>
							<?php endforeach;?>
							</table>
						</div>
					<?php $idx_session++; endforeach;?>
				  <?php endforeach;?>
			</div>
			<div class="col-lg-5">
				<ul class="list-group">
					<?php foreach($data as $ip => $sessions):$idx_session = 1;?>
						<?php foreach($sessions as $session_id => $messages): ?>
							<li target="<?php echo "$ip-$session_id"?>" class="list-group-item item-session <?php echo ((!$messages['success'] && $messages['ended']) ? 'list-group-item-danger' : ($messages['ended'] ? 'list-group-item-success' : ''))?>"><span class="badge"><?php echo count($messages['messages']);?></span><?php echo $ip; echo count($sessions) > 1 ? " #{$idx_session}" : ''?></li>
						<?php $idx_session++; endforeach;?>
				  	<?php endforeach;?>
				</ul>
			</div>
		<?php else: ?>
			<center><h4>Лог пуст.</h4></center>
		<?php endif;?>
	<?php else: ?>
		<center><h4>Лог не найден</h4></center>
	<?php endif;?>